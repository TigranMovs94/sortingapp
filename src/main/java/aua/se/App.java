package aua.se;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        int[] numbers = Arrays.stream(args)
                .mapToInt(Integer::parseInt)
                .sorted()
                .toArray();

        for (int number : numbers) {
            System.out.print(number + " ");
        }
    }

    /**
     * Sorts and formats an array of strings representing integers.
     * @param args the array of string arguments
     * @return a string of sorted integers
     */
    public static String sortAndFormat(String[] args) {
        int[] numbers = Arrays.stream(args)
                .mapToInt(Integer::parseInt)
                .sorted()
                .toArray();

        StringBuilder sb = new StringBuilder();
        for (int number : numbers) {
            sb.append(number).append(" ");
        }
        return sb.toString();
    }
}
