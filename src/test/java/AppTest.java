import aua.se.App;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class AppTest {

    private String input;
    private String expected;

    public AppTest(String input, String expected) {
        this.input = input;
        this.expected = expected;
    }


    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { "", "" },
                { "1", "1 " },
                { "2 1", "1 2 " },
                { "5 4 3 2 1", "1 2 3 4 5 " },
                { "1 2 3 4 5 6 7 8 9 10 11", "1 2 3 4 5 6 7 8 9 10 " }  // More than ten arguments, should ignore extras
        });
    }

    @Test
    public void testSorting() {
        assertEquals(expected, App.sortAndFormat(input.split(" ")));
    }
}
